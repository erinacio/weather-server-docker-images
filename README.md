# 使用方法

## 加载镜像

``gunzip`` 解压为 tar 包，使用 ``docker load`` 加载

## 运行镜像

### fake-api-server

直接运行即可，默认开放 5000 端口，需要自行手动映射

### dashboard-server

需要在容器创建时定义容器的 ``SECRET_KEY`` 环境变量，默认开放 8080 端口，需要自行手动映射。

镜像中打包了初始数据库，初始用户用户名为 ``admin``，密码为 ``loremipsum``
